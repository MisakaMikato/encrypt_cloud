import requests
import random
import itertools
import hashlib
import os

from . import config

base_url = "http://{}:8000/{}"


def init_key(key_id: int):
    for ip in config.NODE_IP_LIST:
        action = 'initkey'
        url = base_url.format(ip, action)
        params = {'key_id': key_id}
        res = requests.get(url, params=params).json()
        if 'success' not in res:
            print('[ERROR] {}:{}'.format(url, res))
            return False
        print('[DEBUG] {}'.format(res))
    return True


def calc_node_share(key_id: int):
    for ip in config.NODE_IP_LIST:
        action = 'calcnodeshare'
        url = base_url.format(ip, action)
        params = {'key_id': key_id}
        res = requests.get(url, params=params).json()
        if 'success' not in res:
            print('[ERROR] {}:{}'.format(url, res))
            return False
        print('[DEBUG] {}'.format(res))

    return True


def get_secret(key_id):
    """获取密钥"""
    shares = []
    for ip in config.NODE_IP_LIST:
        action = 'getshare'
        url = base_url.format(ip, action)
        params = {'key_id': key_id}
        res = requests.get(url, params=params).json()
        if('success' not in res):
            print('[ERROR] {}:{}'.format(url, res))
            return []
        shares.append({
            'id': int(res['node_id']),
            'share': int(res['share'])
        })
    shares = random.choice([x for x in itertools.combinations(shares, 3)])
    return RecoverySecret(shares)


def RecoverySecret(shares: dict):
    """使用拉格朗日插值法计算秘密"""
    # 参与者x<t+1,无法恢复密文
    print("[DEBUG] {}".format(shares))
    secret = 0
    for i in shares:
        fenmu = 1
        fenzi = 1
        for j in shares:
            if(i['id'] == j['id']):
                continue
            fenmu *= (j['id']-i['id'])
            fenzi *= j['id']
        tmp = fenzi*i['share']/fenmu
        secret += tmp % config.MOD_Q
    return round(secret) % config.MOD_Q


def get_secret_md5(key_id: int):
    secret = str(get_secret(key_id)).encode('utf-8')
    m = hashlib.md5()
    m.update(secret)
    hash1 = m.digest()
    m.update(hash1)
    return m.hexdigest()
