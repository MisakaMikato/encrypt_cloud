from django.urls import path, re_path

from . import views

urlpatterns = [
    path('manage/', views.file_view, name='manage'),
    path('upload/', views.upload_view, name='upload'),
    path('<int:file_id>/delete', views.delete_view, name='delete'),
    path('<int:file_id>/download', views.download_view, name='download'),
    re_path(r'^$', views.file_view),
]
