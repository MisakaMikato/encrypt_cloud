import time
import os
import requests

from django.shortcuts import render, redirect
from django.http import HttpRequest, FileResponse

from .models import Files
from key.models import Keys
from .forms import UploadFileForm
from key import api_helper, config
from . import aes_helper

UPLOAD_DIR = "files/upload/"
TMP_DIR = "files/tmp/"


def file_view(request):
    files_model = Files.objects.order_by('id')
    return render(request, "files/manage.html", {'files': files_model})


def upload_view(request: HttpRequest):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            key_model = Keys.objects.get(id=form.cleaned_data.get('key'))
            utcnow = time.time()
            random_name = "{}.enc".format(int(utcnow))
            file_model = Files(
                name=request.FILES['file'].name,
                random_name=utcnow,
                key=key_model
            )
            file_model.save()
            secret_md5 = api_helper.get_secret_md5(key_model.id)
            # encrypt and write to disk
            # paht: upload/<random_name>
            aes_helper.encrypt_file(
                secret_md5, request.FILES.get('file'), random_name)
            # sun_file and upload to node1 and node2
            sub_file(UPLOAD_DIR + random_name)
            return redirect('/files/manage')
    else:
        form = UploadFileForm()
        return render(request, 'files/upload.html', {'form': form})


def download_view(request: HttpRequest, file_id: int):
    file_model = Files.objects.get(id=file_id)
    random_name = file_model.get_random_name()
    download_file_path = TMP_DIR + random_name
    # 从分布节点下载文件并合并, 输出到tmp目录下
    download(random_name, TMP_DIR + random_name)
    # 解密该文件
    secret_md5 = api_helper.get_secret_md5(file_model.key_id)
    print("[DEBUG] secret_md5: {}".format(secret_md5))
    aes_helper.decrypt_file(secret_md5, download_file_path,
                            TMP_DIR + file_model.name)
    # 将文件返回给用户
    f = open(TMP_DIR + file_model.name, 'rb')
    response = FileResponse(f)
    response['Content-Type'] = 'application/octet-stream'
    response['Content-Disposition'] = 'attachment;filename={}'.format(
        file_model.name)
    # 删除本地文件
    if os.path.exists(TMP_DIR + random_name):
        os.remove(TMP_DIR + random_name)
    if os.path.exists(TMP_DIR + file_model.name):
        os.remove(TMP_DIR + file_model.name)
    return response


def delete_view(request: HttpRequest, file_id):
    file_model: Files = Files.objects.get(id=file_id)
    file_path = UPLOAD_DIR + file_model.get_random_name()
    if(os.path.exists(file_path)):
        os.remove(file_path)
    file_model.delete()
    return redirect("/files/manage")


def sub_file(in_file_path):
    cnt = 0
    chunksize = os.path.getsize(in_file_path)//2 + 1
    filename = os.path.basename(in_file_path)
    with open(in_file_path, 'rb') as in_file:
        while True:
            chunk = in_file.read(chunksize)
            if(not chunk):
                break
            tmp_file_path = TMP_DIR + filename + '.sub' + str(cnt)
            out_file = open(tmp_file_path, 'wb')
            out_file.write(chunk)
            out_file.close()
            # 分别将*.sub0和*.sub1上传至node1和node2
            upload(tmp_file_path, config.NODE_IP_LIST[cnt])
            # 上传完后本地删除
            if os.path.exists(tmp_file_path):
                os.remove(tmp_file_path)
            cnt += 1


def upload(in_file_path, node_ip):
    """将临时文件上传到存储节点"""
    url = "http://{}:8000/upload".format(node_ip)
    filename = os.path.basename(in_file_path)
    with open(in_file_path, 'rb') as in_file:
        files = {
            'files': (
                filename,
                in_file,
                'application/octet-stream',
            )
        }
        res = requests.post(url, files=files)
        print('[DEBUG] {}'.format(res.text))


def download(filename, out_file_path):
    """从存储节点下载文件并返回到客户端"""
    base_url = 'http://{}:8000/download?filename={}'
    out_file = open(out_file_path, 'wb')
    for i in range(0, 2):
        url = base_url.format(
            config.NODE_IP_LIST[i], filename+'.sub{}'.format(i))
        res = requests.get(url, stream=True)
        if res.status_code == 200:
            for chunk in res.iter_content(chunk_size=64*1024):
                out_file.write(chunk)
    out_file.close()
