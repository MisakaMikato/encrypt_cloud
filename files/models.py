from django.db import models
from key.models import Keys


class Files(models.Model):
    name = models.CharField(max_length=255)
    random_name = models.IntegerField(default=0)
    key = models.ForeignKey(Keys, on_delete=models.CASCADE)
    upload_time = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.key

    def get_random_name(self):
        return "{}.enc".format(self.random_name)
