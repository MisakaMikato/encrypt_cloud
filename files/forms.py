from django import forms

from key.models import Keys


class UploadFileForm(forms.Form):
    key = forms.ChoiceField(
        label="选择密钥",
        widget=forms.Select(attrs={'class': 'form-control'})
    )
    file = forms.FileField(
        label="上传文件"
    )

    def __init__(self, *args, **kwargs):
        super(UploadFileForm, self).__init__(*args, **kwargs)
        self.fields['key'].choices = (
            ((x.id, x.name) for x in Keys.objects.all()))
