STATIC_URL = '/static/'

CONFIG_DIR = 'key_exchange/config/'
UPLOAD_DIR = 'key_exchange/uploads/'

# 本节点ID
NODE_ID = 1

# 大素数q, 定义运算域
MOD_Q = 587255354717
# 定义参与者数量
PARTICIPANT_N = 5
# 定义多项式阶数
ORDER_T = 2
# 其他节点IP地址
NODE_IP_LIST = ['172.17.0.2', '172.17.0.3',
                '172.17.0.4', '172.17.0.5', '172.17.0.6']
# NODE_IP_LIST = ['127.0.0.1']
SECRET = -1
