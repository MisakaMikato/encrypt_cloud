from django.urls import path, re_path

from . import views

urlpatterns = [
    path('manage/', views.key_view, name='manage'),
    path('add/', views.add_view, name='add'),
    path('<int:key_id>/delete', views.delete_view, name='delete'),
    path('<int:key_id>/get-file-list', views.get_file_list_by_key),
    re_path(r'^$', views.key_view),
]
