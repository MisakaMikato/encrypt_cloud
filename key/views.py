from django.shortcuts import render, redirect
from django.http import JsonResponse

from .models import Keys
from files.models import Files
from . import api_helper


def add_view(request):
    key_model = Keys()
    key_model.save()
    if not api_helper.init_key(key_model.id):
        # return error page
        return render(request, 'key/manage.html')

    api_helper.calc_node_share(key_model.id)
    secret_hash = api_helper.get_secret_md5(key_model.id)
    key_model.name = f'key{key_model.id}'
    key_model.value = secret_hash
    key_model.save()
    return redirect("../manage")


def key_view(request):
    key_model = Keys.objects.order_by("id")
    return render(request, 'key/manage.html', {'keys': key_model})


def delete_view(request, key_id):
    Keys.objects.filter(id=key_id).delete()
    return redirect("../manage")


def get_file_list_by_key(request, key_id):
    file_obj = Files.objects.filter(key_id=key_id)
    file_list = []
    for item in file_obj:
        file_list.append(item.name)
    return JsonResponse({'message': file_list})
