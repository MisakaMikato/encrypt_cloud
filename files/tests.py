from django.test import TestCase
import requests


def download(filename):
    url = 'http://127.0.0.1:8001/download?filename={}'.format(filename)
    res = requests.get(url, stream=True)
    if res.status_code == 200:
        with open(filename, 'wb') as f:
            for chunk in res.iter_content(chunk_size=64*1024):
                f.write(chunk)


if __name__ == "__main__":
    download('files/upload/1583660357.enc.sub0')